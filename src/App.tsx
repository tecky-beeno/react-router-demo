import React, { useEffect, useLayoutEffect, useReducer, useState } from 'react'
import './App.css'
import {
  useHistory,
  useLocation,
  useParams,
  useRouteMatch,
  Route,
  Link,
  Switch,
} from 'react-router-dom'

function App() {
  return (
    <div>
      <nav>
        <Link to="/user">user</Link>
        <Link to="/admin">admin</Link>
      </nav>

      <Switch>
        <Route path="/user" component={UserPage} />
        <Route path="/admin">
          <h2>admin page</h2>
          <nav>
            <Link to="/admin/holiday/record">
              <button>holiday</button>
            </Link>
            <Link to="/admin/personal">
              <button>personal</button>
            </Link>
            <Link to="/admin/departments">
              <button>departments</button>
            </Link>
          </nav>
          <Switch>
            <Route path="/admin" exact>
              admin home page content
            </Route>
            <Route path="/admin/holiday/record" component={HolidayRecord} />
            <Route path="/admin/holiday/apply" component={HolidayApply} />
            <Route path="/admin/personal" component={PersonalPage} />
            <Route path="/admin/departments" component={DepartmentList} />
          </Switch>
        </Route>
      </Switch>
    </div>
  )
}

function UserPage() {
  const history = useHistory()
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  const tab = params.get('tab')
  console.log({ tab })

  function changeTab(name: string) {
    let url = location.pathname + '?tab=' + name
    console.log(url)
    history.push(url)
  }

  return (
    <>
      <h2>user page</h2>
      <p>i am user</p>
      <button onClick={() => changeTab('')}>default</button>
      <button onClick={() => changeTab('1')}>1</button>
      <button onClick={() => changeTab('2')}>2</button>

      {(function () {
        switch (tab) {
          case '1':
            return <User1 />
          case '2':
            return <User2 />
          default:
            return <UserDefault />
        }
      })()}
    </>
  )
}

function UserDefault() {
  return <div>user default</div>
}
function User2() {
  return <div>user 2</div>
}
function User1() {
  return <div>user 1</div>
}

function DepartmentList() {
  const departmentList = [
    { id: 1, name: 'HR' },
    { id: 2, name: 'IT' },
    { id: 3, name: 'AD' },
  ]
  const [staffList, setStaffList] = useState<string[]>([])
  const history = useHistory()
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  const departmentId = +(params.get('id') as string)

  useEffect(() => {
    setStaffList([
      'department ' + departmentId + ' worker 1',
      'department ' + departmentId + ' worker 2',
      'department ' + departmentId + ' worker 3',
    ])
  }, [departmentId])

  function setDepartment(id: number) {
    let params = new URLSearchParams()
    params.set('id', id.toString())
    history.push('/admin/departments?' + params)
  }
  return (
    <>
      <h3>Departments</h3>
      {departmentList.map(department => (
        <div
          className="department-list-item"
          key={department.id}
          onClick={() => {
            setDepartment(department.id)
          }}
        >
          <b>{department.name}</b>
          {departmentId === department.id &&
            staffList.map(staff => (
              <div key={staff} className="department-staff-list-item">
                {staff}
              </div>
            ))}
        </div>
      ))}
    </>
  )
}

function HolidayApply() {
  return (
    <>
      <h3>apply holiday</h3>
      <input type="date" />
    </>
  )
}

function HolidayRecord() {
  return (
    <>
      <h3>holiday record</h3>
      <Link to="/admin/holiday/apply">
        <button>Apply Holiday</button>
      </Link>
      <ol>
        <li>day 1</li>
        <li>day 2</li>
        <li>day 3</li>
      </ol>
    </>
  )
}

function PersonalPage() {
  const history = useHistory()
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  const name = params.get('name')?.toString() || ''
  // console.log(name)
  // const [name, setName] = useState('')

  const [result, setResult] = useState<any>(null)

  function setName(name: string) {
    let params = new URLSearchParams()
    params.set('name', name)
    history.push('/admin/personal?' + params.toString())
  }
  useEffect(() => {
    console.log('searching by name...:', name)
    // let url = decodeURIComponent(location.search)
    // console.log('url:', url);
    fetch('https://postman-echo.com/get' + location.search)
      .then(res => res.json())
      .catch(() => {
        return [
          { id: 1, name: `alice ${name}` },
          { id: 2, name: `bob ${name}` },
          { id: 3, name: `charlie ${name}` },
        ]
      })
      .then(data => setResult(data))
  }, [name, location])
  return (
    <>
      <h3>personal</h3>
      <label htmlFor="search_personal">search personal: </label>
      <input
        id="search_personal"
        value={name}
        onChange={e => setName(e.target.value)}
      />
      <p>Result:</p>
      <pre>
        <code>{JSON.stringify(result, null, 2)}</code>
      </pre>
    </>
  )
}

export default App
